// Creates a card element with name, description, picture, dates, and location
function createCard(name, description, pictureUrl, starts, ends, location) {
  const startDate = new Date(starts).toLocaleDateString('en-US');
  const endDate = new Date(ends).toLocaleDateString('en-US');

  return `
    <div class="card mb-4 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${startDate} - ${endDate}</div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("An error has occurred")
    } else {
      const data = await response.json();

      const columns = document.querySelectorAll('.col');
      let columnIndex = 0;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const location = details.conference.location.name;
          
          // Generates a card element in HTML, appends it to a column and moves onto the next column. 
          // Wraps columns if applicable.
          const html = createCard(name, description, pictureUrl, starts, ends, location);
          columns[columnIndex].innerHTML += html;
          columnIndex = (columnIndex + 1) % columns.length;
        }
      }
    }
  } catch (e) {
    // Logs if/any errors
    console.error(e);

    // Generates HTML error message
    const errorHtml = `
      <div class="alert alert-danger" role="alert">
        Error occurred while fetching conference data: ${e.message}
      </div>
    `;

    // Adds the error message to the column
    const column = document.querySelector('.col');
    column.innerHTML += errorHtml;
  }
});
