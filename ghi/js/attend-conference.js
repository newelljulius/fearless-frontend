window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    const loadingIcon = document.getElementById('loading-conference-spinner');
    const successAlert = document.getElementById('success-message');
    const form = document.getElementById('create-attendee-form');

    loadingIcon.classList.add('d-none');
    selectTag.classList.remove('d-none');
    form.classList.add('d-none');
    successAlert.classList.remove('d-none');
  }
});
